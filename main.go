// This repo illustrates that go run is a bit stupid. By 'stupid' I
// mean not  smart enough to follow go package structure.
//
// The repo has two go files defining a command. The file main.go
// does nothing but print to standard output a value defined in
// support.go.
//
// If clone the repo and, in a terminal, change to the repo's
// directory and then issue the command:
//   go run main.go
// you will see that there is an error reported in main.go: that `val`
// is undefined. If, however, you issue the command:
//   go run *.go
// it works as expected. Likewise, issuing the commands:
//   go build && ./gorunisstupid
// also works as expected.
//
// As near as I can figure, the issue is that the `go run` command is,
// under the covers, running a local copy of the go playround
// (https://play.golang.org/) and that cannot recognize package
// structures.
//
// I have encountered claims that you shouldn't use `go run` to test
// anything other than the simplest of code snippets. This all seems
// to validate that recommendation. Happily, go compiles extremely
// quickly, so the difference between `go run *.go` and `go build &&
// ./<packagename>` is trivial.
//

// The behaviour here reported was observed on
// - go version go1.3.3 linux/amd64 (Debian Jessie)
// - go version go1.10.3 darwin/amd64 (macOS High Sierra 10.13.6)
package main

import (
	"fmt"
)

func main() {
	fmt.Println(val)
}
